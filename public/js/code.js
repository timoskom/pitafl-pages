//PURPLE_500 = 'rgb(0xBB, 0x86, 0xFC ,1.0)';

/*document.styleSheets[0].insertRule(`:root{
    --purple-500: #03DAC5FF;
    --purple-700: #3700B3FF;
    --teal-200: #03DAC5FF;
    --teal-700: #018786FF;
}`);*/

function renderMustache() {
    var home;
    var privacy;
    var privacyNoads;
    var about;
    var repo;
    
    if (screen.width > 600) {
        home = {
            "home": 1
        };
        privacy = {
            "privacy": 1
        };
        privacyNoads = {
            "privacy-noads": 1
        }
        about = {
            "about": 1
        };
        repo = {
            "repo": 1
        };
    } else {
        home = {
            "home": null
        };
        privacy = {
            "privacy": null
        };
        privacyNoads = {
            "privacy-noads": null
        };
        about = {
            "about": null
        };
        repo = {
            "repo": null
        };
    }

    const homeTemplate = document.getElementById('home-template').innerHTML;
    const homeRendered = Mustache.render(homeTemplate, home);
    document.getElementById('navbar-home-item').innerHTML = homeRendered;

    const privacyTemplate = document.getElementById('privacy-template').innerHTML;
    const privacyRendered = Mustache.render(privacyTemplate, privacy);
    document.getElementById('navbar-privacy-item').innerHTML = privacyRendered;

    const privacyNoadsTemplate = document.getElementById('privacy-noads-template').innerHTML;
    const privacyNoadsRendered = Mustache.render(privacyNoadsTemplate, privacyNoads);
    document.getElementById('navbar-privacy-noads-item').innerHTML = privacyNoadsRendered;

    const aboutTemplate = document.getElementById('about-template').innerHTML;
    const aboutRendered = Mustache.render(aboutTemplate, about);
    document.getElementById('navbar-about-item').innerHTML = aboutRendered;

    const repoTemplate = document.getElementById('repo-template').innerHTML;
    const repoRendered = Mustache.render(repoTemplate, repo);
    document.getElementById('navbar-repository-item').innerHTML = repoRendered;
}

$(window).on('load resize', function () {

    document.getElementById('navbar-home-item').innerHTML = `
        <script id="home-template" type="x-tmpl-mustache">
          {{#home}}
            <a href="index.html">
              <i id="home-icon" class="bi bi-house-door-fill"></i>
              Home
            </a>
          {{/home}}
          {{^home}}
            <a href="index.html">
              <i id="home-icon" class="bi bi-house-door-fill"></i>
            </a>
          {{/home}}
        </script>
    `;

    document.getElementById('navbar-privacy-item').innerHTML = `
        <script id="privacy-template" type="x-tmpl-mustache">
          {{#privacy}}
            <a href="privacy.html" onclick="runPrivacy()">
              <i id="privacy-icon" class="bi bi-shield-lock-fill"></i>
              Privacy
            </a>
          {{/privacy}}
          {{^privacy}}
            <a href="privacy.html" onclick="runPrivacy()">
              <i id="privacy-icon" class="bi bi-shield-lock-fill"></i>
            </a>
          {{/privacy}}
        </script>
    `;

    document.getElementById('navbar-privacy-noads-item').innerHTML = `
        <script id="privacy-noads-template" type="x-tmpl-mustache">
          {{#privacy-noads}}
            <a href="privacy-noads.html" onclick="runPrivacyNoads()">
              <i id="privacy-icon" class="bi bi-shield-lock-fill"></i>
              Privacy-Noads
            </a>
          {{/privacy-noads}}
          {{^privacy-noads}}
            <a href="privacy-noads.html" onclick="runPrivacy()">
              <i id="privacy-icon" class="bi bi-shield-lock-fill"></i>
            </a>
          {{/privacy-noads}}
        </script>
    `;

    document.getElementById('navbar-about-item').innerHTML = `
        <script id="about-template" type="x-tmpl-mustache">
          {{#about}}
            <a href="about.html">
              <i id="about-icon" class="bi bi-info-circle-fill"></i>
              About
            </a>
          {{/about}}
          {{^about}}
            <a href="about.html">
              <i id="about-icon" class="bi bi-info-circle-fill"></i>
            </a>
          {{/about}}
        </script>
    `;
  
    document.getElementById('navbar-repository-item').innerHTML = `
        <script id="repo-template" type="x-tmpl-mustache">
          {{#repo}}
            <a href="https://gitlab.com/timoskom/pitafl-pages">
              <i id="repository-icon" class="bi bi-git"></i>
              Repository
            </a>
          {{/repo}}
          {{^repo}}
            <a href="https://gitlab.com/timoskom/pitafl-pages">
              <i id="repository-icon" class="bi bi-git"></i>
            </a>
          {{/repo}}
        </script>
    `;

    renderMustache();
});